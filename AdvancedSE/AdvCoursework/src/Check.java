import java.util.HashMap;
import java.util.HashSet;

public class Check {

	private HashSet<Dish> menu;
	private HashMap<Integer, HashSet<Order>> orders;

	private double checkTotal = 0;

	public Check(HashSet<Dish> menu, HashMap<Integer, HashSet<Order>> orders) {
		super();
		this.menu = menu;
		this.orders = orders;
	}

	public double SumAllmenuOrders(int aTableID) {

		MenuList allMenu = new MenuList(menu);
		OrderList tableOrder = new OrderList(orders);

		double sumOfDish = 0;
		double sumOfAllDishes = 0;

		try {
			for (Order order : orders.get(aTableID)) {
				sumOfDish = order.getQuantum() * allMenu.getHashMapDishesAndPrices().get(order.getDishName());
				sumOfAllDishes += sumOfDish;
				// System.out.println(String.format("%25s%10s%10s%2s",order.getDishName(),order.getQuantity()+"
				// *",allMenu.getHashMapDishesAnsPrices().get(order.getDishName()),"
				// $ = "+sumOfDish+" $"));

			}
		} catch (Exception e) {
			// System.out.println("This Table was not used today");
			// System.exit(0);
		}

		return checkTotal += sumOfAllDishes;

	}

	// Print the Check
	public void printCheck(int aTableID) {

		MenuList allMenu = new MenuList(menu);
		OrderList tableOrders = new OrderList(orders);

		double sumOfDish = 0;
		double sumOfAllDishes = 0;

		System.out.println(String.format("%38s", "Check") + "\n" + String.format("%41s", "==========="));
		System.out.println(String.format("%40s", "Table " + aTableID) + "\n");
		try {
			for (Order order : orders.get(aTableID)) {
				sumOfDish = order.getQuantum() * allMenu.getHashMapDishesAndPrices().get(order.getDishName());
				sumOfAllDishes += sumOfDish;
				System.out.println(String.format("%25s%10s%10s%2s", order.getDishName(), order.getQuantum() + "     *",
						allMenu.getHashMapDishesAndPrices().get(order.getDishName()), " $  =  " + sumOfDish + " $"));

			}
		} catch (Exception e) {
			System.out.println("Table has not been used today");
			// System.exit(0);
		}

		System.out.println("\n");
		System.out.println(String.format("%58s", "Total excluding Tax " + sumOfAllDishes + " $"));
		System.out.println(String.format("%58s", "Tax  " + sumOfAllDishes * 0.23 + " $"));
		System.out.println(String.format("%58s", "Total Check " + sumOfAllDishes * 1.23 + " $"));
	}

	// Print the Check
	public String StringOPrintedCheck(int aTableID) {

		String printedCheck = "";

		MenuList allMenu = new MenuList(menu);
		OrderList tableOrders = new OrderList(orders);

		double sumOfDish = 0;
		double sumOfAllDishes = 0;
		printedCheck += String.format("%38s", "Check") + "\n" + String.format("%41s", "===========");
		printedCheck += String.format("%40s", "Table " + aTableID) + "\n";
		try {
			for (Order order : orders.get(aTableID)) {
				sumOfDish = order.getQuantum() * allMenu.getHashMapDishesAndPrices().get(order.getDishName());
				sumOfAllDishes += sumOfDish;
				printedCheck += String.format("%25s%10s%10s%2s", order.getDishName(), order.getQuantum() + "     *",
						allMenu.getHashMapDishesAndPrices().get(order.getDishName()), " $  =  " + sumOfDish + " $");

			}
		} catch (Exception e) {
			System.out.println("Table has not been used today");
			// System.exit(0);
		}

		printedCheck += "\n";
		printedCheck += String.format("%58s", "Total excluding Tax " + sumOfAllDishes + " $");
		printedCheck += String.format("%58s", "Tax  " + sumOfAllDishes * 0.23 + " $");
		printedCheck += String.format("%58s", "Total Check " + sumOfAllDishes * 1.23 + " $");
		return printedCheck;
	}

}
