
public class Dish implements Comparable{
	
	private String name;
	private double price;
	private String category;
	
	
	//Constructor
	
	public Dish (String name, double price, String category) {
		this.name = name;
		this.price = price;
		this.category = category;
	}
	
	//Gets
	
	public String getName() {
		return name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public String getCategory() {
		return category;
	}
	
	//HashSet Method
	
	public boolean equals(Object other) {
		Dish otherDish = (Dish)other;
		return otherDish.getName().equals(this.getName());
	}
	
	public int hashCode() {
		return name.hashCode();
	}
	
	//Method for Comparable
	
	@Override
	public int compareTo(Object other) {
		Dish otherDish = (Dish)other;
		return name.compareTo(otherDish.getName());
	}

}
