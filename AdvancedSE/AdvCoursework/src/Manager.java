import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;

public class Manager {

	// Main Data Stractures of the Programm
	private HashSet<Dish> menu = new HashSet<Dish>();
	private HashMap<Integer, HashSet<Order>> orders = new HashMap<Integer, HashSet<Order>>();

	public void run() throws FileNotFoundException {

		// Finding Files and Creating the Data Stractures
		File menuFile = new File("menu.txt");
		File ordersFile = new File("order.txt");

		ReadMenu aMenu = new ReadMenu(menuFile);
		menu = aMenu.getMenu();

		ReadOrder allOrders = new ReadOrder(ordersFile, menu);
		orders = allOrders.getOrders();
		
		Check check = new Check(menu,orders);
		Check_GUI checkgui = new Check_GUI(menu, orders);
		check.printCheck(1);

		// Print the Menu in the File The_Menu
		MenuList printedMenu = new MenuList(menu);
		File file1 = new File("the_Menu.txt");
		FileOutputStream fos1 = new FileOutputStream(file1);
		PrintStream ps1 = new PrintStream(fos1);
		System.setOut(ps1);
		printedMenu.printMenu();

		// Print the FrequencyReport
		FrequencyReport freqReport = new FrequencyReport(orders);
		File file2 = new File("Frequency_Report.txt");
		FileOutputStream fos2 = new FileOutputStream(file2);
		PrintStream ps2 = new PrintStream(fos2);
		System.setOut(ps2);
		freqReport.printFrequencyReport();

		// Dishes not Ordered
		FrequencyReport dishesNotOrdered = new FrequencyReport(orders);
		File file3 = new File("Dishes_Not_Ordered.txt");
		FileOutputStream fos3 = new FileOutputStream(file3);
		PrintStream ps3 = new PrintStream(fos3);
		System.setOut(ps3);
		dishesNotOrdered.printDishesNotOrdered();
		


	}

}
