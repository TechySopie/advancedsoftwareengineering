import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

public class MenuList {
	
	private HashSet<Dish> menu;
	
	//Constructor
	public MenuList(HashSet<Dish> menu) {
		super();
		this.menu = menu;
	}
	
	//Get the name of dishes in Strings
	public HashSet<String> getMenuNames() {
		HashSet<String> menuNames = new HashSet<String>();
		for (Dish dish: menu )
		menuNames.add(dish.getName());
		return menuNames;
	}
	
	//Get HashMap
	public HashMap<String, Double> getHashMapDishesAndPrices() {
		HashMap<String,Double> hashMapDishesAndPrices = new HashMap<String, Double>();
		for (Dish dish : menu)
			hashMapDishesAndPrices.put(dish.getName(), dish.getPrice());
			return hashMapDishesAndPrices;
	}
	
	//Printing the menu
	
	public void printMenu() {
	TreeSet<Dish> starterDishes = new TreeSet<Dish>();
	TreeSet<Dish> mainCourseDishes = new TreeSet<Dish>();
	TreeSet<Dish> dessertDishes = new TreeSet<Dish>();
	TreeSet<Dish> beveragesDishes = new TreeSet<Dish>();
	
	//To convert ArrayList into TreeSet
	
	for (Dish dish: menu) {
		if(dish.getCategory().equals("STARTER"))
			starterDishes.add(dish);
		else if(dish.getCategory().equals("MAIN"))
			mainCourseDishes.add(dish);
		else if(dish.getCategory().equals("DESSERT"))
			dessertDishes.add(dish);
		else if(dish.getCategory().equals("DRINKS"))
			beveragesDishes.add(dish);	
	}
	
	System.out.println(String.format("%40s","MENU")+"\n");
	System.out.println(String.format("%43s","===========")+"\n"+"\n");
	System.out.println(String.format("%42s","STARTERS")+"\n"+"\n");
	for(Dish dish: starterDishes)
	System.out.println(String.format("%25s%25s%2s",dish.getName(),dish.getPrice(),"$")+"\n");
	System.out.println( "\n"+String.format("%40s","MAIN")+"\n"+"\n");
	for(Dish dish: mainCourseDishes)
	System.out.println( String.format("%25s%25s%2s",dish.getName(),dish.getPrice(),"$")+"\n");
	System.out.println("\n"+String.format("%42s","DESSERTS")+"\n"+"\n"); 
	for(Dish dish: dessertDishes)
	System.out.println(String.format("%25s%25s%2s",dish.getName(),dish.getPrice(),"$")+"\n") ;
	System.out.println("\n"+String.format("%40s","DRINKS")+"\n"+"\n");
	for(Dish dish: beveragesDishes)
	System.out.println(String.format("%25s%25s%2s",dish.getName(),dish.getPrice(),"$")+"\n");

	}


}
