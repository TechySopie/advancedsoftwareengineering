import java.util.ArrayList;
import java.util.HashSet;

public class NotOrderedDishes {
	
	private ArrayList<Dish> menu;
	private ArrayList<Order> orders;
	
	public NotOrderedDishes(ArrayList<Dish> menu, ArrayList<Order> orders) {
		super();
		this.menu = menu;
		this.orders = orders;
	}
	
	public void printNotOrderedDishes() {
		HashSet<String> menuDishes = new HashSet<String>();
		HashSet<String> orderDishes = new HashSet<String>();
		
		//HashSet to add all name available in menu
		for (Dish dish : menu)
			menuDishes.add(dish.getName());
		
		//Adding the name of the ordered dishes
		for (Order order : orders)
			orderDishes.add(order.getDishName());
		
		//HashSet Differences
		menuDishes.removeAll(orderDishes);
		
		System.out.println(("Not Ordered Dishes" + "\n"+"----------------"+"\n"));
		for(String dName: menuDishes)
			System.out.println(dName);

	}

}
