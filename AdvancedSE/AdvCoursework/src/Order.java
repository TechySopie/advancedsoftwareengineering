
public class Order {
	
	private int id;
	private int tableID;
	private String dishName;
	private int quantum;
	private int counterID = 1;
	
	//Constructor
	
	public Order (int tableID, String dishName, int quantum){
		
		this.tableID = tableID;
		this.dishName = dishName;
		this.quantum = quantum;
		id = counterID++;
	}
	
	//Gets
	
	public int getId() {
		return id;
	}

	public int getTableID() {
		return tableID;
	}
	
	public String getDishName() {
		return dishName;
	}
	
	public int getQuantum() {
		return quantum;
	}
	
	//Set Method
	
	public void setQuantum(int quantum) {
		this.quantum  = quantum;
	}
	
	//Hashset Method
	
	public boolean equals(Object other) {
		Order otherOrder = (Order)other;
		otherOrder.setQuantum(quantum+otherOrder.getQuantum());
		return otherOrder.getDishName().equals(this.getDishName());
	}
	
	public int hashCode() {
		return dishName.hashCode();
	}
}
