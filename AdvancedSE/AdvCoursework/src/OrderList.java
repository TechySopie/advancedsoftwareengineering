import java.util.HashMap;
import java.util.HashSet;

public class OrderList {
	
	private static HashMap<Integer, HashSet<Order>> orders;
	
	//Constructor
	public OrderList(HashMap<Integer, HashSet<Order>> orders) {
		super();
		this.orders = orders;
	}
	
	//Categorizing the Dish Names into Strings by taking the HashSet
	
	public HashSet<String> getHashSetOfStringsOfDishes() {
		HashSet <String> hashSetOfStringsOfDishes = new HashSet<String>();
		for (Integer tableID: orders.keySet()){
			for (Order order: orders.get(tableID))
				hashSetOfStringsOfDishes.add(order.getDishName());
	}
	
		return hashSetOfStringsOfDishes;
}

	
	//Using HashMap for each Table
	public HashMap<String, Integer> getHashMapOfDishNameAndQuantumPerTable(int tableID) {
		HashMap<String, Integer> hashMapOfDishNameAndQuantumPerTable = new HashMap<String, Integer>();
		for( Order order: orders.get(tableID))
			hashMapOfDishNameAndQuantumPerTable.put(order.getDishName(), order.getQuantum());
		return hashMapOfDishNameAndQuantumPerTable;
	}
}