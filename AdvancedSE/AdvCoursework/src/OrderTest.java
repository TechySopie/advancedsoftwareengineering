import static org.junit.Assert.*;

import org.junit.Test;

public class OrderTest {

	@Test
	public void test() {
		
		Order expected1 = new Order(1,"Salmon",1) ;
	
		//checking for orders format
		Order order1= new Order(1,"Salmon",1);
	    Order actual1 = new Order(order1.getTableID(), order1.getDishName(),order1.getQuantum());
		assertEquals(expected1,actual1);
		
		//checking for dishes format
		
		Dish expected=new Dish("Salmon",20,"Starter");
		
		Dish dish1= new Dish("Salmon",20,"Starter");
	    Dish actual = new Dish(dish1.getName(), dish1.getPrice(),dish1.getCategory());
		assertEquals(expected,actual);

	}

}


