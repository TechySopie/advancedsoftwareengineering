import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;

public class ReadMenu {

	private  HashSet<Dish> dishes = new HashSet<Dish>();
	private int errorLineCategory =0;
	private int errorLinePrice = 1;
	private int errorLineOther = 1;

	
	// Constructor
	public ReadMenu(File menu) throws FileNotFoundException {
		
		Scanner scannerMenu = null;

		// it reads every single line in the file
		try {
			scannerMenu = new Scanner(menu);
		} catch (Exception e) {
			System.out.println("Apologise, missing the menu file");
			System.exit(0);
		}
		while (scannerMenu.hasNextLine()) {

			String inputLine = scannerMenu.nextLine();
			String Dishparts[] = inputLine.split(",");
			
			// Variables before implementing try and catch
			
			String dishName="";
			String dsPrice="";
			double dishPrice=0;
			String dishCategory="";
			
			//Catching at the end the try out bounds error 
			try{ 
				// variable that will count on which line is the error
				errorLineOther++;
				
				// Delete any space in front of  dish Name
				dishName=Dishparts[0];

				if (Character.isWhitespace(dishName.charAt(0))) {
					dishName = dishName.replaceFirst("\\s+", "");
				}
				
				/* Converting our String inputs into our preferred variables
				 catch and try for wrong input */

				dsPrice = Dishparts[1];
				dsPrice = dsPrice.trim();
			 
				try{
					dishPrice = Double.parseDouble(dsPrice);
			 
					// counter for error line
					errorLinePrice++;
				} catch (Exception e) {
				System.out.println("Wrong Input in Line " + errorLinePrice + " !" + "\n"
						+ "The program only accepts double variables regarding the Price of Dishes");
				System.exit(0);
			}

			dishCategory = Dishparts[2];
			dishCategory = dishCategory.trim();
			dishCategory = dishCategory.toUpperCase();
			errorLineCategory++;
			if(! (dishCategory.equals("STARTER") || dishCategory.equals("MAIN")|| dishCategory.equals("DESSERT")|| dishCategory.equals("DRINKS") ) ) {
				System.out.println("Wrong Input in Line " + errorLineCategory + " !" + "\n"
						+ "The program only accepts Starter,Main,Dessert or Drinks as variables regarding the Category of Dishes");
				System.exit(0); }

				}catch (Exception e) {
				int errorLineOtherMinusOne = errorLineOther - 1;
				System.out.println("Problem in Line " + errorLineOtherMinusOne + " !" + "\n" + "Check your input again."
						+ "\n" + "Check if you haven�t forgotten any commas. ");
				System.exit(0);
			}
			Dish eachDish = new Dish(dishName, dishPrice, dishCategory);
			dishes.add(eachDish);
		}

	}

	public  HashSet<Dish> getMenu() {
		return dishes;
	}

}
